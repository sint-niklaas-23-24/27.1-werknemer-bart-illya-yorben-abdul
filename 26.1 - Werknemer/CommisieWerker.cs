﻿using System.Windows;
using System.Windows.Media.Imaging;

namespace _26._1___Werknemer
{
    internal class CommisieWerker : Werknemer
    {
        private int _aantal;
        private decimal _commissie;

        public CommisieWerker(string naam, string voornaam, decimal loon, BitmapImage geslacht, decimal commissie, int aantal) : base(naam, voornaam, loon, geslacht)
        {
            Aantal = aantal;
            Commissie = commissie;
        }

        public int Aantal
        {
            get { return _aantal; }
            set
            {
                if (value > 0)
                {
                    _aantal = value;
                }
                else
                {
                    throw new Exception("Aantal commissiewerken mag geen negatieve waarde hebben");
                }
            }
        }
        public decimal Commissie
        {
            get { return _commissie; }
            set
            {
                if (value > 0)
                {
                    _commissie = value;
                }
                else
                {
                    MessageBox.Show("U gaf een negatieve waarde in.", "Negatieve waarde", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        public override decimal Verdiensten()
        {
            return (Loon + Aantal) * Commissie;
        }
        public override string ToString()
        {
            return base.ToString() + " - Commissiewerker";
        }
    }
}
