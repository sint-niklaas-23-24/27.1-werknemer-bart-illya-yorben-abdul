﻿using System.Windows;
using System.Windows.Media.Imaging;

namespace _26._1___Werknemer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            rdoCommissie.IsChecked = true;
            lijstWerknemers = new List<Werknemer>();
            rdoMan.IsChecked = true;
        }
        List<Werknemer> lijstWerknemers;                       //Lijst mag niet in regel 16? Anders dan winforms?
        bool zijnGelijk;
        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            try                                                                         // TryCatch voor de formatexception te handelen
            {
                if (!string.IsNullOrEmpty(txtNaam.Text) && !string.IsNullOrEmpty(txtVoornaam.Text)) //Als het niet leeg is gaan we verder
                {
                    if (!txtNaam.Text.Any(char.IsDigit) && !txtVoornaam.Text.Any(char.IsDigit))     //Als er geen nummerieke waarde is gaan we verder
                    {
                        zijnGelijk = false;
                        string naam = txtNaam.Text;                                                 //Variabelen maken op line 21... is cleaner?
                        string voornaam = txtVoornaam.Text;
                        decimal loon = Convert.ToDecimal(txtLoon.Text);
                        BitmapImage geslachtImage = new BitmapImage();

                        if (rdoMan.IsChecked == true)
                        {
                            geslachtImage = new BitmapImage(new Uri("/images/mannelijk_teken.png", UriKind.Relative));
                        }
                        else
                        {
                            geslachtImage = new BitmapImage(new Uri("/images/mannelijk_teken.png", UriKind.Relative));

                        }

                        Werknemer equalsWerknemer = new Werknemer(naam, voornaam, loon, geslachtImage);
                        if (lijstWerknemers.Count == 0)                                             //Als het leeg is, gewoon uitvoeren
                        {
                            MedewerkerToevoegen();                                                  //Code hergebruiken > Methode
                            zijnGelijk = false;                                                     //Niet nodig denkik , maar failsafe
                        }
                        else
                        {
                            foreach (Werknemer werknemer in lijstWerknemers)                        //Foreach om elke werknemer in onze lijst als object te zetten
                            {
                                if (Equals(equalsWerknemer, werknemer))                             //Als naam+voornaam in de objecten overeen komen dan mogen we niet toevoegen
                                {
                                    MessageBox.Show("Deze werknemer met deze naam & voornaam bestaat al, deze werd niet toegevoegd.", "Gebruiker bestaat al", MessageBoxButton.OK, MessageBoxImage.Error);
                                    zijnGelijk = true;
                                }

                            }
                            if (zijnGelijk == false)                                                //Mag niet in een foreach gebeuren, gebruiken een bool 
                            {
                                MedewerkerToevoegen();

                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Vul een naam en voornaam in zonder een numerieke waarde (bv. Gilles Fabry)", "Getal in naam", MessageBoxButton.OK, MessageBoxImage.Error);
                        txtNaam.Clear(); txtVoornaam.Clear();

                    }
                }
                else
                {
                    MessageBox.Show("Vul een naam en voornaam in. (bv. Gilles Fabry)", "Naam/Voornaam zijn leeg", MessageBoxButton.OK, MessageBoxImage.Error);

                }
            }
            catch (FormatException)                                                     //Vangt verkeerde input, lege input op, negatieve waardes worden gehandeld in properties
            {
                MessageBox.Show("Vul een decimaal getal in bij de velden loon & uur(bv:3 of 3.5) en vul enkel een geheel getal in bij aantal (bv:5).", "Foutieve waarde", MessageBoxButton.OK, MessageBoxImage.Error);
                txtCommissie.Clear();
                txtLoon.Clear();
                txtAantal.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //Fields en textvakken hiden/displayen gebaseerd op welke knop er geselecteerd is.
        private void rdoWerknemer_Checked(object sender, RoutedEventArgs e)
        {
            lblCommissie.IsEnabled = false;
            txtCommissie.IsEnabled = false;
            txtCommissie.Visibility = Visibility.Collapsed;
            lblCommissie.Visibility = Visibility.Collapsed;
            lblAantal.IsEnabled = false;
            txtAantal.IsEnabled = false;
            lblAantal.Visibility = Visibility.Collapsed;
            txtAantal.Visibility = Visibility.Collapsed;
        }

        private void rdoUurwerk_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.IsEnabled = true;
            txtAantal.IsEnabled = true;
            lblAantal.Visibility = Visibility.Visible;
            txtAantal.Visibility = Visibility.Visible;
            lblCommissie.IsEnabled = false;
            txtCommissie.IsEnabled = false;
            txtCommissie.Visibility = Visibility.Collapsed;
            lblCommissie.Visibility = Visibility.Collapsed;
        }

        private void rdoCommissie_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.IsEnabled = true;
            txtAantal.IsEnabled = true;
            lblAantal.Visibility = Visibility.Visible;
            txtAantal.Visibility = Visibility.Visible;
            lblCommissie.IsEnabled = true;
            txtCommissie.IsEnabled = true;
            lblCommissie.Visibility = Visibility.Visible;
            txtCommissie.Visibility = Visibility.Visible;

        }

        private void rdoStukwerker_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.IsEnabled = true;
            txtAantal.IsEnabled = true;
            lblAantal.Visibility = Visibility.Visible;
            txtAantal.Visibility = Visibility.Visible;
            lblCommissie.IsEnabled = false;
            txtCommissie.IsEnabled = false;
            txtCommissie.Visibility = Visibility.Collapsed;
            lblCommissie.Visibility = Visibility.Collapsed;
        }
        //Methodes
        public void MedewerkerToevoegen() // Kijkt naar de button , voegt de medewerker toe, ruimt medewerkers zonder loon op, displayed dan de medewerkers.
        {
            string naam = txtNaam.Text;
            string voornaam = txtVoornaam.Text;
            decimal loon = Convert.ToDecimal(txtLoon.Text);
            BitmapImage geslachtImage = new BitmapImage();

            if (rdoMan.IsChecked == true)
            {
                geslachtImage = new BitmapImage(new Uri("/images/mannelijk_teken.png", UriKind.Relative));
            }
            else
            {
                geslachtImage = new BitmapImage(new Uri("/images/mannelijk_teken.png", UriKind.Relative));

            }

            Werknemer werknemer;                       // Aanmaken hier
            if (rdoCommissie.IsChecked == true)
            {
                int aantal = Convert.ToInt32(txtAantal.Text);
                decimal commissie = Convert.ToDecimal(txtCommissie.Text);
                werknemer = new CommisieWerker(naam, voornaam, loon, geslachtImage, commissie, aantal);  // elke commissiewerker is een werknemer
                //lijstWerknemers.Add(nieuweComWerker); //Overbodig                            ^Beter
            }
            else if (rdoWerknemer.IsChecked == true)
            {
                werknemer = new Werknemer(naam, voornaam, loon, geslachtImage); // elke werknemer is een werknemer
                //lijstWerknemers.Add(nieuweWerknemer); //Overbodig     ^Beter
            }
            else if (rdoUurwerk.IsChecked == true)
            {
                int aantal = Convert.ToInt32(txtAantal.Text);
                werknemer = new Uurwerker(naam, voornaam, loon, geslachtImage, aantal); // elke uurwerker is een werknemer
                //lijstWerknemers.Add(nieuweUurwerker); //Overbodig           ^Beter
            }
            else
            {
                int aantal = Convert.ToInt32(txtAantal.Text);
                werknemer = new StukWerker(naam, voornaam, loon, geslachtImage, aantal);// elke stukwerker is een werknemer
                //lijstWerknemers.Add(nieuweStukwerker); //Overbodig         ^Beter
            }
            lijstWerknemers.Add(werknemer);
            WerknemersDisplayen();
        }

        public void WerknemersDisplayen() //Laat alle werknemers zien in de txtdisplay.
        {
            lbGegevensDisplay.ItemsSource = null;
            lbGegevensDisplay.ItemsSource = lijstWerknemers;
            txtNaam.Clear();
            txtVoornaam.Clear();
            txtAantal.Clear();
            txtCommissie.Clear();
            txtLoon.Clear();
        }

    }
}