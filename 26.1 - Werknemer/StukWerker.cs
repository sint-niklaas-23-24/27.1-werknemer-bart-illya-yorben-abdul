﻿using System.Windows.Media.Imaging;

namespace _26._1___Werknemer
{
    internal class StukWerker : Werknemer
    {
        private int _aantal;

        public StukWerker(string naam, string voornaam, decimal loonPerStuk, BitmapImage geslacht, int aantal) : base(naam, voornaam, loonPerStuk, geslacht)
        {
            this.Aantal = aantal;
        }

        public int Aantal
        {
            get { return _aantal; }
            set
            {
                if (value > 0)
                {
                    _aantal = value;
                }
                else
                {
                    throw new Exception("Aantal stukken werk mag geen negatieve waarde hebben");
                }
            }
        }
        public override decimal Verdiensten()
        {
            return Loon * Aantal; //Base mag ook
        }
        public override string ToString()
        {
            return base.ToString() + " - Stukwerker";
        }
    }
}
