﻿using System.Windows.Media.Imaging;

namespace _26._1___Werknemer
{
    internal class Uurwerker : Werknemer
    {
        private double _uren;

        public Uurwerker(string naam, string voornaam, decimal loon, BitmapImage geslacht, double aantalUren) : base(naam, voornaam, loon, geslacht)
        {
            this.Uren = aantalUren;
        }
        public double Uren
        {
            get { return _uren; }
            set
            {
                if (value > 0)
                {
                    _uren = value;
                }
                else
                {
                    throw new Exception("Aantal uren werk mag geen negatieve waarde hebben");
                }
            }
        }
        public override decimal Verdiensten()
        {
            return Loon * Convert.ToDecimal(Uren); //Base mag ook ( return base.Verdiensten()*Convert.ToDecimal(Uren)
        }
        public override string ToString()
        {
            return base.ToString() + " - Uurwerker";
        }
    }
}
