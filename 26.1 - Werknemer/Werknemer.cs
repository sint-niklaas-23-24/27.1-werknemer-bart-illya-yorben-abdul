﻿using System.Windows.Media.Imaging;

namespace _26._1___Werknemer
{
    internal class Werknemer
    {
        private decimal _loon;
        private string _naam;
        private string _voornaam;
        private BitmapImage _geslacht;
        public Werknemer() { }
        public Werknemer(string naam, string voornaam, decimal loon, BitmapImage geslacht)
        {
            this.Naam = naam;
            this.Voornaam = voornaam;
            this.Loon = loon;
            this.Geslacht = geslacht;
        }
        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        public string Gegevens
        {
            get { return this.ToString(); }
        }
        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }
        public decimal Loon
        {
            get { return _loon; }
            set
            {
                if (value > 0)
                {
                    _loon = value;
                }
                else
                {
                    throw new Exception("Het loon mag geen negatieve waarde hebben");
                }
            }
        }
        public virtual decimal Verdiensten()
        {
            return Loon;
        }
        public BitmapImage Geslacht
        {
            get { return _geslacht; }
            set { _geslacht = value; }
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Werknemer g = (Werknemer)obj;
                    if (this.Naam == g.Naam && this.Voornaam == g.Voornaam)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {

            return $"{Naam.PadRight(20)} {Voornaam.PadRight(20)} (€{Verdiensten()})"; //Werkte niet
        }
    }
}
